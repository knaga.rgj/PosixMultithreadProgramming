
all: posix-multithread-programming.pdfv

.PRECIOUS: posix-multithread-programming.pdf

%.pdf:%.adoc
	asciidoctor-pdf  -r asciidoctor-diagram -r asciidoctor-mathematical $<

%.pdfv:%.pdf
	qpdfview $<

%.xpdf:%.pdf
	xpdf $<


clean:
	/bin/rm -f *~ stem-*svg adoc-style/*~
	/bin/rm -rf  .asciidoctor
	/bin/rm -f sample0.svg sin0.svg graph0.svg
