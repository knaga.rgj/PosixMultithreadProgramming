<!-- [GitLab版 GFM](https://docs.gitlab.com/ee/user/markdown.html)   -->
<!-- [GitHub版 GFM](https://github.github.com/gfm/) -->

# PosixMultithreadProgramming

Please execute `git submodule update --init --recursive` at first.

This repository uses my [AdocStyle](https://gitlab.com/knaga.rgj/AdocStyle) project as a submodule, and AdocStyle uses my [GenX Pseudo Italic](https://gitlab.com/knaga.rgj/GenX-Pseudo-Italic).
