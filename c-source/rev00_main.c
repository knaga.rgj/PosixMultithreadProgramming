//
// rev00_main.c
//   single thread
//

#include <stdio.h>  // fputs,fgets,fprintf
#include <string.h> // strtok

// each command for interpriter
void Status(void);
void Light(int is_on);

const char *dlmtr = " ,;\t\r\n";

int main(void)
{
  fputs("-- start main loop\n", stderr);
  while(1) {
    char line[256];
    fputs("> ", stderr);
    fgets(line, 256, stdin);

    char *tok = strtok(line, dlmtr);
    if (tok==NULL) continue;

    if (strcmp(tok, "status")== 0) {
      Status();

    } else if (strcmp(tok, "light") == 0) {
      tok = strtok(NULL, dlmtr);
      if (tok!=NULL) {
        if (strcmp(tok, "on") == 0) {
          Light(1);
        } else if (strcmp(tok, "off") == 0) {
          Light(0);
        } else {
          fprintf(stderr, "** argument error light {on|off}, got '%s'\n", tok);
        }
      } else {
        fputs("** argument error light needs 'on' or 'off'\n", stderr);
      }

    } else if (strcmp(tok, "quit") == 0) {
      break;
      
    } else {
      fprintf(stderr, "** unknown command '%s'\n",tok);
    }      

  } // while(1)

  return 0;
}

//
// each command for interpriter
//
void Status(void)
{
  fputs("print status...\n", stdout);
}


void Light(int is_on)
{
  if (is_on == 1) {
    fputs("light on\n", stdout);
  } else {
    fputs("light off\n", stdout);
  }
}


/*****************************************************************************
  Copyright (c) 2021, knaga.rgj@gmail.com
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  * Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the organization nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
