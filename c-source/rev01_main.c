//
// rev01_main.c
//   launch a worker thread
//

#include <stdio.h>   // fputs(),fgets(),fprintf()
#include <string.h>  // strtok(), strerror()
#include <pthread.h> // pthread_*()
#include <signal.h>  // signal(), sigfillset(), pthread_sigmask()
#include <stdlib.h>  // exit()
#include <unistd.h>  // sleep()
#include <errno.h>   // errno

// each command for interpriter
void Status(void);
void Light(int is_on);

// worker thread
void *WorkerProc(void *args);

// exit handler
void CleanExit(int sig);

// shared variables (for signal handler)
pthread_t thr;
int is_worker_launched;


const char *dlmtr = " ,;\t\r\n";

int main(void)
{
  // initialize (signal handler)
  is_worker_launched=0;

  // setup signal handler
  signal(SIGINT, CleanExit);  // Ctrl-C
  signal(SIGTERM, CleanExit); // The default signal for kill command is TERM.

  
  // launch worker thread
  int ret = pthread_create(&thr, NULL, WorkerProc, NULL);
  if (ret != 0) {
    fprintf(stderr, "** failed to pthread_create(): %s\n",
            strerror(errno));
    return -1;
  }
  is_worker_launched=1;
  fputs("-- main(): launced worker\n", stderr);

  // main loop
  fputs("-- start main loop\n", stderr);
  while(1) {
    char line[256];
    fputs("> ", stderr);
    fgets(line, 256, stdin);

    char *tok = strtok(line, dlmtr);
    if (tok==NULL) continue;

    if (strcmp(tok, "status")== 0) {
      Status();

    } else if (strcmp(tok, "light") == 0) {
      tok = strtok(NULL, dlmtr);
      if (tok!=NULL) {
        if (strcmp(tok, "on") == 0) {
          Light(1);
        } else if (strcmp(tok, "off") == 0) {
          Light(0);
        } else {
          fprintf(stderr, "** argument error light {on|off}, got '%s'\n", tok);
        }
      } else {
        fputs("** argument error light needs 'on' or 'off'\n", stderr);
      }

    } else if (strcmp(tok, "quit") == 0) {
      break;
      
    } else {
      fprintf(stderr, "** unknown command '%s'\n",tok);
    }      

  } // while(1)

  CleanExit(0);

  return 0;
}

//
// each command for interpriter
//
void Status(void)
{
  fprintf(stdout, "is_worker_launched: %d\n", is_worker_launched);
}


void Light(int is_on)
{
  if (is_on == 1) {
    fputs("light on\n", stdout);
  } else {
    fputs("light off\n", stdout);
  }
}

//
// worker thread
//
void *WorkerProc(void *args)
{
  // block all signals
  sigset_t all_sigs;
  sigfillset(&all_sigs);
  pthread_sigmask(SIG_BLOCK, &all_sigs, NULL);
  
  fputs("-- start Worker loop\n", stderr);
  while(1) {
    sleep(1);
    fputs("work\n", stdout);
  }
}

//
// exit handler
//
void CleanExit(int sig)
{
  if (is_worker_launched) {
    pthread_cancel(thr);
    pthread_join(thr, NULL);
    is_worker_launched=0;
    fputs("-- CleanExit(): Worker joined\n", stderr);
  }
  exit(0);
}



/*****************************************************************************
  Copyright (c) 2021, knaga.rgj@gmail.com
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  * Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the organization nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
