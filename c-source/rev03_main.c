//
// rev03_main.c
//   launch a worker thread
//   control worker process from main()
//   cancelpoint control
//

#include <stdio.h>   // fputs(),fgets(),fprintf()
#include <string.h>  // strtok(), strerror()
#include <pthread.h> // pthread_*()
#include <signal.h>  // signal(), sigfillset(), pthread_sigmask()
#include <stdlib.h>  // exit()
#include <unistd.h>  // sleep()
#include <errno.h>   // errno

// type definition
enum WorkerControl {NOP=0, START, STOP};
enum WorkerStatus {UNKNOWN=0, READY, RUNNING};

// each command for interpriter
void Status(void);
void ControlWorker(enum WorkerControl control);

// worker thread
void *WorkerProc(void *args);

// exit handler
void CleanExit(int sig);

// shared variables (for signal handler)
pthread_t thr;
int is_worker_launched;

// shared variables (for main() and Worker())
pthread_mutex_t lock;
pthread_cond_t cvar;
enum WorkerControl worker_control;
enum WorkerStatus worker_status;


const char *dlmtr = " ,;\t\r\n";

int main(void)
{
  // initialize (signal handler)
  is_worker_launched=0;

  // setup signal handler
  signal(SIGINT, CleanExit);  // Ctrl-C
  signal(SIGTERM, CleanExit); // The default signal for kill command is TERM.

  // initialize (woker thread)
  pthread_mutex_init(&lock, NULL);
  pthread_cond_init(&cvar, NULL);
  worker_control = NOP;
  worker_status = UNKNOWN;
  
  
  // launch worker thread
  int ret = pthread_create(&thr, NULL, WorkerProc, NULL);
  if (ret != 0) {
    fprintf(stderr, "** failed to pthread_create(): %s\n",
            strerror(errno));
    return -1;
  }
  is_worker_launched=1;
  fputs("-- main(): launced worker\n", stderr);

  // main loop
  fputs("-- start main loop\n", stderr);
  while(1) {
    char line[256];
    fputs("> ", stderr);
    fgets(line, 256, stdin);

    char *tok = strtok(line, dlmtr);
    if (tok==NULL) continue;

    if (strcmp(tok, "status")== 0) {
      Status();

    } else if (strcmp(tok, "worker") == 0) {
      tok = strtok(NULL, dlmtr);
      if (tok!=NULL) {
        if (strcmp(tok, "on") == 0) {
          ControlWorker(START);
        } else if (strcmp(tok, "off") == 0) {
          ControlWorker(STOP);
        } else {
          fprintf(stderr, "** argument error worker {on|off}, got '%s'\n", tok);
        }
      } else {
        fputs("** argument error worker needs 'on' or 'off'\n", stderr);
      }

    } else if (strcmp(tok, "quit") == 0) {
      break;
      
    } else {
      fprintf(stderr, "** unknown command '%s'\n",tok);
    }      

  } // while(1)

  CleanExit(0);

  return 0;
}

//
// each command for interpriter
//
void Status(void)
{
  pthread_mutex_lock(&lock);
  enum WorkerStatus stat = worker_status;
  pthread_mutex_unlock(&lock);

  const char *stat_str;
  switch(stat) {
    case UNKNOWN: stat_str = "Unknown"; break;
    case READY:   stat_str = "Ready";   break;
    case RUNNING: stat_str = "Running"; break;
    default:      stat_str = "Unexpected"; break;
  }
  
  fprintf(stdout, "is_worker_launched: %d\n", is_worker_launched);
  fprintf(stdout, "worker_status: %s\n", stat_str);
}


void ControlWorker(enum WorkerControl control)
{
  pthread_mutex_lock(&lock);
  worker_control = control;
  pthread_cond_signal(&cvar);
  pthread_mutex_unlock(&lock);

  const char *str;
  switch(control) {
    case NOP:   str="Nop";   break;
    case START: str="Start"; break;
    case STOP:  str="Stop";  break;
    default:    str="Unexpected"; break;
  }
  fprintf(stderr, "-- ControlWorker(): sets %s\n", str);
}

//
// worker thread
//
void *WorkerProc(void *args)
{
  // block cancel
  pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);

  // block all signals
  sigset_t all_sigs;
  sigfillset(&all_sigs);
  pthread_sigmask(SIG_BLOCK, &all_sigs, NULL);

  pthread_mutex_lock(&lock);
  worker_status = READY;
  // pthread_cond_signal(&cvar); // comment-outed as noone waits for status-change-event
  pthread_mutex_unlock(&lock);
  
  
  fputs("-- start Worker loop\n", stderr);
  while(1) {
    fputs("-- worker waits for start command.\n", stderr);
    pthread_mutex_lock(&lock);
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
    while(worker_control != START) pthread_cond_wait(&cvar, &lock);
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
    worker_status = RUNNING;
    pthread_mutex_unlock(&lock);

    while(1) {
      pthread_mutex_lock(&lock);
      pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
      pthread_testcancel();
      pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
      if (worker_control != START) {
        worker_status = READY;
        pthread_mutex_unlock(&lock);
        break;
      }
      pthread_mutex_unlock(&lock);
      fputs("work\n", stdout);
      sleep(1);
    }
  }
}

//
// exit handler
//
void CleanExit(int sig)
{
  if (is_worker_launched) {
    pthread_cancel(thr);
    pthread_join(thr, NULL);
    is_worker_launched=0;
    fputs("-- CleanExit(): Worker joined\n", stderr);
  }

  if (pthread_mutex_trylock(&lock) != 0) {
    fprintf(stderr, "-- mutex is locked after cancel.\n");
  } else {
    fprintf(stderr, "** unexpected, mutex is unlocked after cancel.\n");
  }

  // release resources after pthread_join()
  pthread_cond_destroy(&cvar);
  pthread_mutex_destroy(&lock);
  
  exit(0);
}



/*****************************************************************************
  Copyright (c) 2021, knaga.rgj@gmail.com
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  * Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of the organization nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
