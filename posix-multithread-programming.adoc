= POSIX マルチスレッドプログラミング
// [注意] ヘッダ部で空行を入れるとバグる
K.Naga <knaga.rgj@gmail.com>
v 0.4, 2021/04/12
// タイトル行の下と、フッタの右側に Creative Common を表示、不要ならコメントアウト
:organization: 帰宅部
:creative-commons: CC BY-NC-SA
:creative-commons-url: https://creativecommons.org/licenses/by-nc-sa/4.0/
// フッタの左側にコピーライト関連表示
:copyright-holder: {author}
:copyright-year: 2021
// 標準設定読み込み
include::adoc-style/pdf-article-attrs.adoc[]
//:toc:


// article では HTML 同様 「H1 がタイトル」であり「タイトルブロック」ではない
// 下記のように別途右寄せで「本文」で書く
[.text-right]
{organization} +
mailto:{email}[{author}] +
{creative-commons-url}[{creative-commons}]

== はじめに

POSIX は UNIX 系の API の規格で、本書ではその中からマルチスレッド関連の関数を使用したサンプルをいくつか紹介します。

サンプルプログラムは、実用性利便性よりも API の使い方をわかりやすく例示するために作成しているので、その旨ご了承ください。

POSIX は規格であり、細かいところで OS ごとの違いがあるので、必ず OS 付属のマニュアルをよく読むようにしてください。

サンプルは「機能変更に伴いソースコードを改変する」ように作成しているので、差分を可視化するツール (xxdiff や meld など) を活用すると理解の助けになるでしょう。

== プログラミング上の注意

本書で書かれていることをマスターした後には、以下の項目に注意して設計できるようになるといいと思います。
未修者は現時点ではまだ理解できなくとも、この後の例を通じて身につけていってください。

* ワーカースレッドでは基本的にシグナルブロックとすると後々便利
* ワーカースレッドでは基本的に「キャンセルを受け付けてよい場所」のみキャンセルを受け付けるようにすると後々便利
* ワーカースレッドで確保したリソースは、原則としてキャンセルハンドラで開放する。
* スレッドの作成をした後は「いつどこの処理が実行されるか」わからない。そもそも「いつ処理が開始されるか」もわからない。
* マルチスレッド間の共有データは原則として排他制御（例外がないわけではない）
* 共有データの初期化処理はスレッド作成の前に済ませ、リソース開放処理はスレッド合流（join）の後
** mutex や codition variable も共有データ
** スレッド引数も共有データ
* test-and-set, read-modify-write はアトミックに処理する
* ロックで他のスレッドをブロックしている時間は短くなるように、ローカル変数を活用する

<<<

== rev 0: single thread

ユーザと対話的に処理をするプログラム

`status`, `light on`, `light off`, `quit` を処理するプログラムです。

`Ctrl-C` また `quit` コマンドで終了するように設計しました。


[source,c,linenums]
----
include::c-source/rev00_main.c[]
----

<<<

== rev 1: create/cancel/join

pthread_create() でバックグラウンドワーカースレッドを起動します。

プログラム終了時にスレッドも停止させるようにします。
Ctrl-C でプログラム終了する際にも「スレッド停止処理」を実行したいので、シグナルハンドラを活用します。
Ctrl-C 入力時にはプロセスに SIGINT が送られるので、SIGINT に対するハンドラを登録します。
また、kill コマンドで強制終了させるケースも考慮にいれ、kill コマンドが送るデフォルトシグナル SIGTERM に対してもハンドラも登録しておきます。

プロセスに送られるシグナルは「どのスレッドが受け取って処理されるのか」が時の運になってしまうので、ワーカースレッドではシグナルを受け取らないようにしておきます。

試しに「main() でシグナルを全てブロックして、ワーカースレッドがシグナルを受け取る」ようにしてみると、ワーカスレッドがハンドラを実行し、自分に対してキャンセルを発行することになり、おかしなことになることがわかると思います。


main() と CleanExit() で共有する変数はグローバル領域に配置します。残念なことに `pthread_t thr;`  を参照しても「pthread_create() を呼ぶ前か後か」あるいは「読んだけどエラーは起きてないか」を知ることができないので、別途 `int is_worker_launched;` を用意しています。


[source,c,linenums]
----
include::c-source/rev01_main.c[]
----

<<<

== rev 2: mutex, cond_var (スレッド間同期)

ワーカスレッドの処理の開始・停止をコマンド入力で制御できるようにしてみましょう。
今回の例では pthread_craete()/cancel() で開始・停止を制御するのと大差ないですが、ワーカスレッドの初期化処理部でリソースを確保する際に時間がかかったりシステムに負荷をかける場合には、最初に一旦リソースを確保しておいて、開始・停止をすばやく応答させるために、今回の例のような設計が活きてくると思います。

スレッド間同期には condition variable と mutex lock を使います。

「開始指示 - 停止状態」「開始指示 - 動作状態」「停止指示 - 動作状態」「停止指示 - 停止状態」の４つの状態があるので、１つの変数で４状態を管理するようにしても構いませんが、応用がきく設計としては「開始指示 or 停止指示」と「停止状態 or 動作状態」とを２つの変数で管理する方がいいかと思います。

それぞれの変数に対して個別に condvar と mutex を割り当ててもいいですし、２つの変数をまとめて１組の condvar と mutex で管理するのもアリです。
ここでは消費リソースをケチる方向で考えて１組の condvar と mutex で２つの変数を管理することにします。

注意しなければならないのは、スレッド間共有データにおいて、リソースの取得/開放が必要な場合は、create() 前に取得し、join() 後に解放する必要があります。
mutex と condvar も共有データであり、この原則に基づきます。

なおコマンドは `status`, `worker on`, `worker off`, `quit` に変更しています。


[source,c,linenums]
----
include::c-source/rev02_main.c[]
----

<<<

== rev 3: cancelstate

基本的に pthread_cancel() は「キャンセルポイント」と呼ばれる処理内でハンドリングされます。多くは sleep() や sigwait() などの「待ち系」の処理です。

しかしアプリケーションの書き方によっては「mutex lock をロックした状態でキャンセルされ、誰もロック解除できなくなる」ケースもありえます。

最も手軽で効果的な解決方法は「原則キャンセルの受付を停止して、特定の部分だけキャンセルを受け付ける」ように設計することで、pthread_setcancelstate() を使用することです。

この例では「start コマンドを待つ間」と「ループ処理の先頭」で行うこととします。 +
「start コマンドを待つ間」は pthread_cond_wait() で実現し、この関数は「戻るときは mutex lock はロックされた状態」でああるため、キャンセルされた後は mutex lock がロックされた状態となります。 +
「ループ処理の先頭」でキャンセルを受け付けるための pthread_testcancel() においても同じようにしておく方がよいので、pthread_testcance() を実行する前に lock するように設計しています。

[source,c,linenums]
----
include::c-source/rev03_main.c[]
----

<<<


== rev 4: cleanup_push/pop

ワーカースレッドがリソースを別途必要とし、スレッド終了時、特にキャンセルされた時にも開放する必要がある場合は、pthread_cleanup_push()/pthread_cleanup_pop() を利用します。
ただし、この cleanup_push()/pop() はマクロで実現されているため、以下のような地味に面倒な制約があり、またコンパイルエラーもわかりにくいものになります。
* push()/pop() は対で記述しなければならない。
* push()/pop() の間で作成されたコードブロック (`{...}`)は、pus()/pop()間で閉じてなければならない。(push() と pop() の間の `{` と `}` は同数でなければならない。)

この例ではスレッドが malloc() で得たメモリ領域をキャンセルハンドラで free() することでメモリリークを防ぎ、
また、キャンセルポイントでロックされた mutex lock をキャンセルハンドラ内で解除しています。

[source,c,linenums]
----
include::c-source/rev04_main.c[]
----

<<<


== rev 5: periodic timer & scheduling

その１: timer を signal で

ワーカースレッドが一定周期で処理を行ために、POSIX timer と signal を利用します。

コンセプトはそれほど難しくないですが、セットアップのコーディングは面倒です。

キャンセルは rev 4 同様、「condwait() で受けるので、mutex はロックしたい状態で統一する」という方針とし、
「sigwait() ではキャンセルを受け付けない」ように設計します。

* 初期化処理
  * pthread_sigmask(): タイマで使用する signal をブロックするようにします。使用する signal は１つですが sigset_t 型で指定します。
  * timer_create(): タイマ(timer_t 型)を作成します。紐付ける signal の情報は struct sigevent 型で指定します。
  * timer_settime(): タイマを開始します。struct itimerspec 型でタイマ発火周期と最初の発火までの時間を指定します。
* 周期処理
  * sigwait(): signal 待ちします。使用する signal は１つですが sigset_t 型で指定します。

設計時は以下の点に注意してください。

* 全スレッドがタイマで使用する signal をブロックするようにします。
* sigwait() 系は「signal をブロックした状態」で呼びます。


その２： scheduling

リアルタイムアプリケーションを構築する際に「このスレッドは他より優先させて実行させる必要がある」ようなケースがあります。

このようなときには pthread_setschedparam() で「ポリシー」と「優先度」を変更するようにします。

対象となるスレッドの処理が周期実行で間欠的に動作させることがわかっている場合、SCHED_FIFO 指定でコンテクストスイッチを
抑制することでシステムパフォーマンスや効率を上げることが期待できます。

一方で、比較的長い処理を平等に実行させるような時には、SCHED_RR を指定することで、適宜切り替えながら処理されるようになります。

SCHED_FIFO も SCHED_RR も優先度が高いものが実行可能な場合はそちらが優先実行されます。
同じ優先度でポリシーが異なる場合の挙動は POSIX では規定されておらず、OS の実装依存になります。



[source,c,linenums]
----
include::c-source/rev05_main.c[]
----

<<<


== anti 1: スレッド引数

スレッドに引数を与える際に一時変数を使うと不具合の原因になりえます。（動いてしまうこともあります）

「スレッド引数は共有データ」であり「join の後で解放」するか「スレッド間同期を利用」するかしてください。

誤り例: １つの共有データ thrarg を複数のスレッドで共有している
[source,c,linenums]
----
   struct ThrArgs thrargs;
   for (int i=0; i<kThrNum; i++) {
     thrargs.x = i*100;
     thrargs.y = i*200;
     pthread_create(&(thr[i]), NULL, WorkerFunc, &thrargs);
   }
----

誤り例: 引数 thrarg[] (共有データ) が create 直後に開放されている
[source,c,linenums]
----
  {
    struct ThrArgs thrargs[kThrNum];
    for (int i=0; i<kThrNum; i++) {
      thrargs[i].x = i*100;
      thrargs[i].y = i*200;
      pthread_create(&(thr[i]), NULL, WorkerFunc, &(thrargs[i]));
    }
  }
----

正解例: スレッド間同期を埋め込む（WorkerFunc() が is_active を 1 にする)
[source,c,linenums]
----
  struct ThrArgs {
    int x;
    int y;
    volatile int is_active;
  };

  {
    struct ThrArgs thrargs[kThrNum];
    for (int i=0; i<kThrNum; i++) {
      thrargs[i].x = i*100;
      thrargs[i].y = i*200;
      thrargs[i].is_active = 0;
      pthread_create(&(thr[i]), NULL, WorkerFunc, &(thrargs[i]));
    }
    for (int i=0; i<kThrNum; i++) {
      while(thrargs[i].is_active == 0) usleep(100000); // 100 ms
    }
  }
----

引数データをグローバル領域やヒープ領域に配置して、リソース開放しないようにする方策でもよいでしょう。


<<<

== anti 2: キャンセルポイント

デフォルト設定では「キャンセルポイントでのみキャンセル処理をハンドリングする」となっているので、
ワーカスレッドがキャンセルポイントなしにループ処理をする場合は外部から止めることができません。

誤り例: キャンセルポイントがない
[source,c,linenums]
----
  while(p<fibo_num) {
    fibo[p] = fibo[p-1] + fibo[p-2];
    p++;
  }
----

正解例: キャンセルポイントを埋め込む
[source,c,linenums]
----
  while(p<fibo_num) {
    pthread_testcancel();
    fibo[p] = fibo[p-1] + fibo[p-2];
    p++;
  }
----

正解例: 非同期キャンセルにする
[source,c,linenums]
----
  pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
  while(p<fibo_num) {
    fibo[p] = fibo[p-1] + fibo[p-2];
    p++;
  }
----

<<<


== appendix 1: mutex lock

基本

[cols="5,4,6"]
|===
^|関数名 ^|概要 ^| man

| pthread_mutex_init()
| 初期化
| https://man7.org/linux/man-pages/man3/pthread_mutex_init.3p.html

| pthread_mutex_destroy()
| 解放
| https://man7.org/linux/man-pages/man3/pthread_mutex_destroy.3p.html

| pthread_mutex_lock()
| ロック
| https://man7.org/linux/man-pages/man3/pthread_mutex_lock.3p.html

| pthread_mutex_unlock()
| ロック解除
| https://man7.org/linux/man-pages/man3/pthread_mutex_unlock.3p.html

| pthread_mutex_trylock()
| ロックしてみて、すでにロック済ならエラーで返る
| https://man7.org/linux/man-pages/man3/pthread_mutex_trylock.3p.html

| pthread_mutex_timedlock()
| ロックしてみて、すでにロック済なら指定時刻を過ぎるまで待ち、過ぎたらエラーで返る。
| https://man7.org/linux/man-pages/man3/pthread_mutex_timedlock.3p.html


| pthread_mutexattr_init()
| 初期化
| https://man7.org/linux/man-pages/man3/pthread_mutexattr_destroy.3p.html

| pthread_mutexattr_setpshared()
| 共有メモリ上に mutex lock を配置してプロセス間で共有する際に使用する
| https://man7.org/linux/man-pages/man3/pthread_mutexattr_setpshared.3.html

| pthread_mutexattr_settype()
| recursive などの mutex の種類を指定する
| https://man7.org/linux/man-pages/man3/pthread_mutexattr_settype.3p.html


|===


`pthread_mutex_timedlock()` は condition variable と異なり、CLOCK_REALTIME しか使用できなさそう。

<<<


== appendix 2: recursive mutex

デフォルトの mutex の挙動は「あるスレッドがロックした mutex をそのスレッドが再びロックするとデッドロックを起こす」ようになっているが、
recursive にすると「同一のスレッドであれば何回でもロックできる。ただし解除も同数実行しなればならない」となる。
`pthread_mutexattr_settype()`  を使用する。

ただし、condition variable とあわせて使用するときは recursive であってはならない。

https://man7.org/linux/man-pages/man3/pthread_mutexattr_settype.3p.html +
https://man7.org/linux/man-pages/man3/pthread_mutexattr_gettype.3p.html

[source,c,linenums]
----
  pthread_mutexattr_t mattr;
  pthread_mutexattr_init(&mattr);
  pthread_mutexattr_settype(&mattr, PTHREAD_MUTEX_RECURSIVE);

  pthread_mutex_t mlock;
  pthread_mutex_init(&mlock, &mattr);

  int ret = phtread_mutex_lock(&mlock); // will be 0
  printf("locked %d\n", ret);
  ret = phtread_mutex_lock(&mlock); // will be 0
  printf("locked again %d\n", ret);
  
  ret = phtread_mutex_unlock(&mlock); // will be 0
  printf("unlocked once %d\n", ret);
  ret = phtread_mutex_unlock(&mlock); // will be 0
  printf("unlocked again %d\n", ret);
  ret = phtread_mutex_unlock(&mlock); // will be non-zero
  printf("unlocked third %d\n", ret);
----

<<<


== appendix 3: condition variable

複数のスレッド間で共有する変数があり、その状態の変化させる際にあわせてイベント通知するための関数群

待機側の基本処理

. mutex lock をロックする
. 「共有変数の状態が希望のものでなければ待機する」を繰り返す。
. 必要であれば共有変数を操作する
. mutex lock をロック解除する

2 の「待機」で使用する `pthread_cond_wait()` `pthread_cond_timedwait()` は mutex lock も引数として与え、
「関数を呼んだ先で自動的にロックが解除されて待機状態になり、signal や broadcast で起きた後は再びロックして戻る」
ようになっている。

この２つの関数は「必ずロックされた状態で戻る」という仕様になっていて、また２つとも cancel point にもなっているため、
ここの待機中に外部から pthread_cancel() を呼ばれると mutex lock がロックされた状態でスレッドはキャンセル処理されてしまうので、
「キャンセルによるスレッドの終了」機能の設計には注意を要する。（<<rev 4: cleanup_push/pop>> 参照)

なお、2. の「繰り返す」の部分は「while をつかうものだ」と暗記しておくほうが後々トラブルに悩まされずに済むように思う。

通知側の基本処理

. mutex lock をロックする
. 共有変数を変更する
. 通知を発行する
. mutex lock をロック解除する


[cols="5,4,6"]
|===
^|関数名 ^|概要 ^| man

| pthread_cond_init()
| 初期化
| https://man7.org/linux/man-pages/man3/pthread_cond_init.3p.html

| pthread_cond_destroy()
| 解放
| https://man7.org/linux/man-pages/man3/pthread_cond_destroy.3p.html

| pthread_cond_signal()
| 通知を発信する。いずれか１つが起きる。
| https://man7.org/linux/man-pages/man3/pthread_cond_signal.3p.html

| pthread_cond_broadcast()
| 通知を発信する。全てが起きる。
| https://man7.org/linux/man-pages/man3/pthread_cond_broadcast.3p.html

| pthread_cond_wait()
| 通知が来るまで待機する
| https://man7.org/linux/man-pages/man3/pthread_cond_wait.3p.html

| pthread_cond_timedwait()
| 指定時刻が来るまで通知を待つ。時刻を過ぎたらエラーで返る
| https://man7.org/linux/man-pages/man3/pthread_cond_timedwait.3p.html

| pthread_condattr_init()
| 初期化
| https://man7.org/linux/man-pages/man3/pthread_condattr_init.3p.html

| pthread_condattr_setclock()
| timedwait する際の時計を変更する。デフォルトは CLOCK_REALTIME が使用される。
| https://man7.org/linux/man-pages/man3/pthread_condattr_setclock.3p.html

| pthread_condattr_setpshared()
| 共有メモリ上に condition variable を配置してプロセス間で共有する際に使用する
| https://man7.org/linux/man-pages/man3/pthread_condattr_setpshared.3p.html

|===


初期化の例
[source,c,linenums]
----
  // 共有変数
  int sem_count;
  pthread_mutex_t mlock;
  pthread_cond_t cvar;

  // 初期化
  sem_count=0;
  pthread_mutex_init(&mlock, NULL);
  pthread_cond_init(&cvar, NULL);
----

待機側の例 (sem_wait)
[source,c,linenums]
----
  pthread_mutex_lock(&mlock);
  while(sem_count==0) { pthread_cond_wait(&cvar, &mlock); }
  sem_count -= 1;
  pthread_mutex_unlock(&mlock);
----

通知側の例 (sem_post)
[source,c,linenums]
----
  pthread_mutex_lock(&mlock);
  sem_count += 1;
  pthread_cond_signal&cvar);
  pthread_mutex_unlock(&mlock);
----
  
<<<


== appendix 4: signal

signal は「プロセス間で伝達」され、「スレッド間で伝達」もされます。
signal は「割り込み処理」のように「現在の処理を一時中断し、signal ハンドラの処理を実行し、
もとの処理に戻って再開する」ためのイベント伝達手段です。
デフォルトのままでは「どのスレッドが signal を受けるかわからない」ので、「ワーカスレッドは
signal を全てブロック」して「main スレッドで割り込み処理を行う」ように設計するとトラブルが少ないと思います。

signal には整数で区別しますが、シンボルが POSIX で割り当てられており、実際の番号は各 OS で異なってもシンボルを使うことで、
OS 非依存のアプリケーションが開発できるようになっています。

ただ、古い規格ではアプリケーションで使用して良い signal は SIGUSR1 と SIGUSR2 の２つだけで不十分で、
また、優先度やキューイングがなされないため、後の規格で「リアルタイムシグナル SIGRTMIN〜SIGRTMAX」が追加されました。

POSIX タイマでは signal を伝達手段として指定できます。周期処理を signal ハンドラの形で記述するのは不便なので、
処理の中で「sigwait() で明示的に signal が到着するまで待つ」ように設計します。

その際、BLOCK 設定（マスク設定）することで「sigwait() が呼ばれる前に signal が到達しても割り込み処理は保留される」ようになり、
sigwait() は BLOCK された signal を拾って戻ります。




設計時の注意点は

* どの signal の割り込み処理を行うか検討する。
* どのスレッドが割り込み処理を行うか検討し、それ以外のスレッドは割り込みを block するようにする。
* タイマ用の signal は block して使用する。


[cols="2,5,6"]
|===
^|関数名 ^|概要 ^| man

| 
| 各 signal の説明
| https://man7.org/linux/man-pages/man7/signal.7.html

| signal()
| signal ハンドラの登録
| https://man7.org/linux/man-pages/man2/signal.2.html

| kill()
| 指定プロセスに指定 signal を送る
| https://man7.org/linux/man-pages/man2/kill.2.html

| sigemptyset()
| sigset_t をゼロクリアする
| https://man7.org/linux/man-pages/man3/sigemptyset.3p.html  https://man7.org/linux/man-pages/man3/sigemptyset.3.html

| sigfllset()
| sigset_t に全ての signal をセットする
| https://man7.org/linux/man-pages/man3/sigfillset.3p.html https://man7.org/linux/man-pages/man3/sigfillset.3.html

| sigaddset()
| setset_t に１つ signal を追加する
| https://man7.org/linux/man-pages/man3/sigaddset.3p.html https://man7.org/linux/man-pages/man3/sigaddset.3.html

| sigwait()
| signal を受け取るまで待つ
| https://man7.org/linux/man-pages/man3/sigwait.3p.html https://man7.org/linux/man-pages/man3/sigwait.3.html

| sigtimedwait()
| 指定時間 signal を待つ（時間がすぎるか signal を受けたら戻る）
| https://man7.org/linux/man-pages/man3/sigtimedwait.3p.html

|===

(※) sigtimedwait() は時刻でなく時間を指定する

<<<



== appendix 5: POSIX timer

POSIX timer ではシグナル以外の伝達手段も考慮して規定されており、struct sigevent型で指定する。


[cols="3,8,7"]
|===
^|関数名 ^|概要 ^| man

| timer_create()
| イベントとタイマを紐づけて初期化する
| https://man7.org/linux/man-pages/man3/timer_create.3p.html

| timer_settime()
| タイマの発火周期と最初の発火時期を指定する
| https://man7.org/linux/man-pages/man3/timer_settime.3p.html

| struct sigevent
| signal を含むイベントを扱う構造体
| https://man7.org/linux/man-pages/man7/sigevent.7.html

|===

// <<<


== appendix 6: scheduling

POSIX ではポリシーとして SCHED_FIFO, SCHED_RR, SCHED_OTHER の３つが規定されている。(要確認)

[cols="1,5"]
|===
^|ポリシー ^|概要

| SCHED_FIFO
| I/O 待ちなどで待機状態になるまで、あるいはより優先度の高い処理が実行権を奪うまで、実行し続ける。

| SCHED_RR
| 一定時間処理したら実行権を放棄し、同じ優先度で実行可能なスレッドに実行権を渡す。

| SCHED_OTHER
| 通常のスケジューリング（非リアルタイム処理用）

|===


[cols="4,4,6"]
|===
^|関数名 ^|概要 ^| man

| pthread_setschedparam() 
| スケジューリングポリシーと、優先度などのパラメータを設定する
| https://man7.org/linux/man-pages/man3/pthread_setschedparam.3.html

| sched_get_prirority_max()
| 指定されたポリシーでの実行優先度の最大値（最優先）を返す
| https://man7.org/linux/man-pages/man2/sched_get_priority_max.2.html

| sched_get_prirority_min()
| 指定されたポリシーでの実行優先度の最小値（最劣位）を返す
| https://man7.org/linux/man-pages/man2/sched_get_priority_min.2.html

| sched_yield()
| 実行を中断し、同じポリシーで同じ優先度の他のスレッドに実行権を譲る
| https://man7.org/linux/man-pages/man2/sched_yield.2.html

|===

<<<

// Local Variables:
// eval: (adoc-mode)
// eval: (outline-minor-mode)
// End:
